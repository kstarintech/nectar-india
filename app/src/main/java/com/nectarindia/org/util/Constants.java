package com.nectarindia.org.util;

import java.util.Locale;

public class Constants {

	// Common Constants
	public static final String BASE_URL = "http://nectarindia-kstartech.rhcloud.com";

	// App Constants

	public static final String GET_MEDICINE_SUB_URL = "/getnectarmedicine.php";


	// Shared preferences file name
	public static final String PREF_NAME = "NectarIndiaLogin";

	public static final String KEY_IS_LOGGED_IN = "isLoggedIn";
	public static  String DOCTORID = "";
}