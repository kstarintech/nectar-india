package com.nectarindia.org.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nectarindia.org.fragments.DetailsViewFragment;
import com.nectarindia.org.fragments.PatientInfoFragment;
import com.nectarindia.org.fragments.PrescriptionFragment;

import java.util.List;

public class MainTabsPagerAdapter extends FragmentPagerAdapter {

	List<Fragment> frags ;
	public MainTabsPagerAdapter(FragmentManager fm, List<Fragment> frag) {

		super(fm);
		frags=frag;
	}

	@Override
	public Fragment getItem(int index) {
		return  frags.get(index);
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 3;
	}

}
