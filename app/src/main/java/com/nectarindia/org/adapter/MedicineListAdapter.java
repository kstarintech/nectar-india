package com.nectarindia.org.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nectarindia.org.R;
import com.nectarindia.org.model.MedicineListModel;

import java.util.ArrayList;

public class MedicineListAdapter extends ArrayAdapter<MedicineListModel> {

	private String TAG = "MedicineListAdapter";
	// declaring our ArrayList of MedicinesList
	private ArrayList<MedicineListModel> objects;
	private Context mContext;
	/* here we must override the constructor for ArrayAdapter
	* the only variable we care about now is ArrayList<TicketDetails> objects,
	* because it is the list of objects we want to display.
	*/
	public MedicineListAdapter(Context context, int textViewResourceId, ArrayList<MedicineListModel> objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
		mContext = context;
	}

	public void setData(ArrayList<MedicineListModel> objects){
		this.objects = objects;
	}
	
	/*
	 * we are overriding the getView method here - this is what defines how each
	 * list TicketDetails will look.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent){

		// assign the view we are converting to a local variable
		View v = convertView;

		// first check to see if the view is null. if so, we have to inflate it.
		// to inflate it basically means to render, or show, the view.
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.medicinelist_row_layout, null);
		}

		//ProductLightBean productList = (ProductLightBean) objects.get(position);
		//ArrayList<MedicineListModel> madicineList = objects;

		//if (madicineList != null) {

			// This is how you obtain a reference to the TextViews.
			// These TextViews are created in the XML files we defined.
			TextView sno = (TextView) v.findViewById(R.id.snoTextView);
			TextView madName = (TextView) v.findViewById(R.id.madicineNameTextView);
			TextView noOfDays = (TextView) v.findViewById(R.id.noOfDayaTextView);
			TextView timings = (TextView) v.findViewById(R.id.timingsTextView);

			sno.setText((position+1)+".");
			madName.setText(objects.get(position).getMedicineName());
			noOfDays.setText(objects.get(position).getNoOfDays()+" Days");
			timings.setText(objects.get(position).getTimings());
			/*Log.e(TAG, "getMedicineName: "+madicineList.get(position).getMedicineName()+" No of days: "+madicineList.get(position).getNoOfDays()
			+" timings: "+madicineList.get(position).getTimings());*/
		//}

		 v.setBackgroundColor(position % 2 == 0 ?  Color.rgb(246, 246, 246)  : mContext.getResources().getColor(R.color.white));
		// the view must be returned to our activity
		return v;

	}


}
