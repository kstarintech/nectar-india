package com.nectarindia.org.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nectarindia.org.R;

import java.io.File;

/**
 * Created by Srinivas Mannem on 14-12-2015.
 */
public class UploadPrescriptionActivity extends Activity{

    private String TAG = "UploadPrescriptionActivity";
    EditText patientInput;
    Button smsBtn,image_share;
    Context mContext;
    private int PICK_IMAGE_REQUEST = 1;
    String picturePath=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_layout);
        mContext = this;

        patientInput = (EditText) findViewById(R.id.patientInput);
        smsBtn = (Button) findViewById(R.id.smsButton);
        image_share = (Button)findViewById(R.id.image_share);

        smsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSms();
            }
        });

        image_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(mContext, "Sharing Image Activity is in progress", Toast.LENGTH_LONG).show();

            image_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    // Show only images, no videos or anything else
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    // Always show the chooser (if there are multiple options available)
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                }
            });
            }
        });
    }

    public void sendSms(){
        Log.e(TAG, "Send SMS");
        String str = patientInput.getText().toString().trim();
        if(str!= null || !str.equals("")){
            String separator = "; ";
            if(android.os.Build.MANUFACTURER.equalsIgnoreCase("samsung")){
                separator = ", ";
            }
            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
            smsIntent.setData(Uri.parse("smsto:"));
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("address", "9243449243");
            smsIntent.putExtra("sms_body", str);

            try {
                startActivity(smsIntent);
                //finish();
                Log.i("Finished sending SMS...", "");
            }
            catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(mContext,
                        "SMS faild, please try again later.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please enter prescription data.!", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("InlinedApi")
    protected void sendEmail() {
        String[] TO = {"praveen@spacesanddeals.com"};
        String[] CC = {""};
        /*String str = patientInput.getText().toString().trim();
        if(str!= null || !str.equals("")){
            str= str;
        } else {
            str = " ";
        }*/
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        Uri U = null;
        if(picturePath!=null) {
            File F = new File(picturePath);
            U = Uri.fromFile(F);
        }

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("*/*");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Prescription Image");
        //emailIntent.putExtra(Intent.EXTRA_TEXT, str);
        emailIntent.putExtra(Intent.EXTRA_STREAM, U);
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send Mail"));
            //finish();
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(mContext, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
        //Toast.makeText(mContext, getResources().getString(R.string.faq_share_email_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            String[] projection = { MediaStore.Images.Media.DATA };

            Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
            cursor.moveToFirst();

            Log.d(TAG, DatabaseUtils.dumpCursorToString(cursor));

            int columnIndex = cursor.getColumnIndex(projection[0]);
            picturePath = cursor.getString(columnIndex); // returns null
            cursor.close();
        }
        sendEmail();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
