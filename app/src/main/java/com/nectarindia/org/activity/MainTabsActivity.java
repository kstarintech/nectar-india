package com.nectarindia.org.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.nectarindia.org.R;
import com.nectarindia.org.adapter.MainTabsPagerAdapter;
import com.nectarindia.org.fragments.DetailsViewFragment;
import com.nectarindia.org.fragments.PatientInfoFragment;
import com.nectarindia.org.fragments.PrescriptionFragment;
import com.nectarindia.org.helper.SQLiteHandler;
import com.nectarindia.org.helper.SessionManager;

import java.util.ArrayList;
import java.util.List;


public class MainTabsActivity extends ActionBarActivity implements
		ActionBar.TabListener {
	List<Fragment> frag=new ArrayList<Fragment>();
	private ViewPager viewPager;
	private MainTabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	private SQLiteHandler db;
	private SessionManager session;
	// Tab titles
	private String[] tabs = { "Patient", "Prescription", "View" };

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_pager);
		LayoutInflater mInflater = LayoutInflater.from(this);
		// Initilization
		viewPager = (ViewPager) findViewById(R.id.viewpager);
		actionBar = getSupportActionBar();

		frag.add(new PatientInfoFragment());
		frag.add(new PrescriptionFragment());
		frag.add(new DetailsViewFragment());
		mAdapter = new MainTabsPagerAdapter(getSupportFragmentManager(),frag);
		viewPager.setAdapter(mAdapter);

		//disable swipe
		final View touchView = findViewById(R.id.viewpager);
		touchView.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				return true; }
		});

		actionBar.setDisplayShowHomeEnabled(false);  // hides action bar icon
		actionBar.setDisplayShowTitleEnabled(false); // hides action bar title
		View mCustomView = mInflater.inflate(R.layout.header, null);
		actionBar.setCustomView(mCustomView);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Adding Tabs
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}

		viewPager.setOffscreenPageLimit(0);
		viewPager.setDrawingCacheEnabled(false);
		viewPager.setDuplicateParentStateEnabled(false);
		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				actionBar.setSelectedNavigationItem(position);
				mAdapter.notifyDataSetChanged();
				invalidateOptionsMenu();
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) { }

			@Override
			public void onPageScrollStateChanged(int arg0) { }
		});

		// SqLite database handler
		db = new SQLiteHandler(getApplicationContext());
		// session manager
		session = new SessionManager(getApplicationContext());
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) { }

	@Override
	public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {	}

	public void setCurrentPosition(int pos){
		viewPager.setCurrentItem(pos);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_item, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// action with ID logout was selected
			case R.id.action_logout:
				logoutUser();
				break;
			default:
				break;
		}
		return true;
	}

	/**
	 * Logging out the user. Will set isLoggedIn flag to false in shared
	 * preferences Clears the user data from sqlite users table
	 * */
	private void logoutUser() {
		session.setLogin(false);
		db.deleteUsers();
		// Launching the login activity
		Intent intent = new Intent(MainTabsActivity.this, LoginActivity.class);
		startActivity(intent);
		finish();
	}

	public void reflectChange() {

		((DetailsViewFragment)frag.get(2)).reflectChange();
	}
}
