package com.nectarindia.org.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nectarindia.org.model.MedicineListModel;

import java.util.ArrayList;

/**
 * Created by Srinivas Mannem on 14-11-2015.
 */
public class SQLiteController {
    private String TAG = "SQLiteController";
    private Context mContext;
    private SQLiteDatabase database;
    private LocalDatabaseHelper dbHelper;

    public SQLiteController(Context aContext){
        mContext = aContext;
    }

    public SQLiteController open() throws SQLException {
        dbHelper = new LocalDatabaseHelper(mContext);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(String name, String noOfDays, String timing) {
        Log.e(TAG, "name: " + name + ":noOfDays: " + noOfDays + ":timing: " + timing);
        open();
        ContentValues contentValue = new ContentValues();
        contentValue.put(dbHelper.KEY_NAME, name);
        contentValue.put(dbHelper.KEY_NO_DAYS, noOfDays);
        contentValue.put(dbHelper.KEY_TIMINGS, timing);

        database.insert(dbHelper.TABLE_MEDICINES, null, contentValue);
        close();
    }

    public ArrayList<MedicineListModel> getAllMedicine() {
        ArrayList<MedicineListModel> medicineList = new ArrayList<MedicineListModel>();

        // Open database for Read / Write
        open();
        // Select All Query

        String selectQuery = "SELECT  * FROM " + dbHelper.TABLE_MEDICINES;


        Cursor cursor = database.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MedicineListModel data = new MedicineListModel();
                data.setId(cursor.getString(0));
                data.setMedicineName(cursor.getString(1));
                data.setNoOfDays(cursor.getString(2));
                data.setTimings(cursor.getString(3));

                // Adding contact to list
                medicineList.add(data);
            } while (cursor.moveToNext());
        }
        Log.e(TAG, "medicineList size: " + medicineList.size());
        close();
        return medicineList;
    }

    public Cursor fetch() {
        String[] columns = new String[] { dbHelper.KEY_ID, dbHelper.KEY_NAME,
                dbHelper.KEY_NO_DAYS, dbHelper.KEY_TIMINGS };
        Cursor cursor = database.query(dbHelper.TABLE_MEDICINES, columns, null,
                null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

   /* public int update(long _id, String name, String desc) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBhelper.TODO_SUBJECT, name);
        contentValues.put(DBhelper.TODO_DESC, desc);
        int i = database.update(DBhelper.TABLE_NAME, contentValues,
                DBhelper._ID + " = " + _id, null);
        return i;
    }*/

    public void delete(String name) {
        open();
        database.delete(dbHelper.TABLE_MEDICINES, dbHelper.KEY_NAME + "=" + "'"+name+"'", null);
        close();
    }
}
