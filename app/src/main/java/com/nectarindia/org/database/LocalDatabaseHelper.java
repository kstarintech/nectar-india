package com.nectarindia.org.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Srinivas Mannem on 14-11-2015.
 */
public class LocalDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "prescription.db";
    private static final int DATABASE_VERSION = 1;
    // Table name
    public static final String TABLE_MEDICINES = "medicines";

    // Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "medicine_name";
    public static final String KEY_NO_DAYS = "no_of_days";
    public static final String KEY_TIMINGS = "timing";

    String CREATE_MADICINES_TABLE = "CREATE TABLE " + TABLE_MEDICINES + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT NOT NULL,"
            + KEY_NO_DAYS + " TEXT NOT NULL," + KEY_TIMINGS + " TEXT" + ")";

    public LocalDatabaseHelper(Context aContext){
        super(aContext, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_MADICINES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDICINES);

        // Create tables again
        onCreate(db);
    }
}
