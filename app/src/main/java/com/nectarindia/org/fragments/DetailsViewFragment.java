package com.nectarindia.org.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nectarindia.org.R;
import com.nectarindia.org.activity.MainTabsActivity;
import com.nectarindia.org.adapter.MedicineListAdapter;
import com.nectarindia.org.database.SQLiteController;
import com.nectarindia.org.listeners.SwipeDismissListViewTouchListener;
import com.nectarindia.org.model.MedicineListModel;
import com.nectarindia.org.util.Constants;

import java.util.ArrayList;

public class DetailsViewFragment extends Fragment {

	private  String TAG = "DetailsViewFragment";
	private  SQLiteController dbController;
	 ArrayList<MedicineListModel> data;
	 Context mContext;
	//ArrayAdapter<String> mAdapter;
	private  MedicineListAdapter medicineListAdapter;
	private  ListView medicineListView;
	static Button smsBtn, shareBtn, btnBack;
	SharedPreferences pref;
	String name, phoneNo, email;
	static TextView no_medicines_tv;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		View rootView = inflater.inflate(R.layout.medicine_list_layout, container, false);
		medicineListView = (ListView) rootView.findViewById(R.id.medicineListView);
		btnBack= (Button) rootView.findViewById(R.id.btn_back);
		smsBtn = (Button) rootView.findViewById(R.id.smsButton);
		shareBtn = (Button) rootView.findViewById(R.id.shareButton);
		no_medicines_tv = (TextView) rootView.findViewById(R.id.no_medicines_tv);
		/*dbController = new SQLiteController(mContext);

		// Reading all madicines
		Log.d("Reading: ", "Reading all madicines..");
		data = dbController.getAllMedicine();

		if(data.size()>0) {
			medicineListView.setVisibility(View.VISIBLE);
			medicineListAdapter = new MedicineListAdapter(mContext,
					R.layout.medicinelist_row_layout, data);
			medicineListAdapter.setNotifyOnChange(true);
			medicineListView.setAdapter(medicineListAdapter);
		} else {
			no_medicines_tv.setVisibility(View.VISIBLE);
			smsBtn.setVisibility(View.GONE);
			shareBtn.setVisibility(View.GONE);
		}*/

		// Create a ListView-specific touch listener. ListViews are given special treatment because
		// by default they handle touches for their list items... i.e. they're in charge of drawing
		// the pressed state (the list selector), handling list item clicks, etc.
		SwipeDismissListViewTouchListener touchListener =
				new SwipeDismissListViewTouchListener(
						medicineListView,
						new SwipeDismissListViewTouchListener.DismissCallbacks() {
							@Override
							public boolean canDismiss(int position) {
								return true;
							}

							@Override
							public void onDismiss(ListView listView, int[] reverseSortedPositions) { }

							@Override
							public void onDismissed(ListView listView, int position) {
								    MedicineListModel data = medicineListAdapter.getItem(position);
									medicineListAdapter.remove(data);
								    dbController.delete(data.getMedicineName());
									medicineListAdapter.notifyDataSetChanged();
							}
						});
		medicineListView.setOnTouchListener(touchListener);
		// Setting this scroll listener is required to ensure that during ListView scrolling,
		// we don't look for swipes.
		medicineListView.setOnScrollListener(touchListener.makeScrollListener());

		smsBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendSms();
			}
		});

		shareBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendEmail();
			}
		});

		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainTabsActivity) getActivity()).setCurrentPosition(1);
			}
		});

		getUserInfoFrmPreferences();

		return rootView;
	}

	public void getUserInfoFrmPreferences(){
		pref = mContext.getSharedPreferences(Constants.PREF_NAME, 0);
		SharedPreferences.Editor editor = pref.edit();

		name = pref.getString("PAT_NAME", "");
		phoneNo = pref.getString("PAT_PHONE","");
		try {
			email = pref.getString("PAT_EMAIL", "");
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		Log.e(TAG, "from preferences::::::name: "+name+",phoneNo: "+phoneNo);
	}

	public void sendSms(){
		Log.e(TAG, "Send SMS");
		String str = formSmsData();
		String separator = "; ";
		if(android.os.Build.MANUFACTURER.equalsIgnoreCase("samsung")){
			separator = ", ";
		}
		//String[] phoneNos = {phoneNo,"9243449243"};
		Intent smsIntent = new Intent(Intent.ACTION_VIEW);
		//Intent smsIntent = new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:5551212;5551212"));
		smsIntent.setData(Uri.parse("smsto:"));
		smsIntent.setType("vnd.android-dir/mms-sms");
		smsIntent.putExtra("address", phoneNo+separator+"9243449243");
		smsIntent.putExtra("sms_body", str);

		try {
			startActivity(smsIntent);
			//finish();
			Log.i("Finished sending SMS...", "");
		}
		catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(mContext,
					"SMS faild, please try again later.", Toast.LENGTH_SHORT).show();
		}

	}

	@SuppressLint("InlinedApi")
	protected void sendEmail() {
		String[] TO = {"praveen@spacesanddeals.com", email};
		String[] CC = {""};

		String str = formEmailData();

		Intent emailIntent = new Intent(Intent.ACTION_SEND);

		emailIntent.setData(Uri.parse("mailto:"));
		emailIntent.setType("text/html");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
		emailIntent.putExtra(Intent.EXTRA_CC, CC);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Medicines Data");
		emailIntent.putExtra(Intent.EXTRA_TEXT, str);
		emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		try {
			mContext.startActivity(Intent.createChooser(emailIntent, "Send Mail"));
			//finish();
		}
		catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(mContext, "There is no email client installed.", Toast.LENGTH_SHORT).show();
		}
		//Toast.makeText(mContext, getResources().getString(R.string.faq_share_email_success), Toast.LENGTH_SHORT).show();
	}

	public String formSmsData(){
		data = dbController.getAllMedicine();
		StringBuffer sb = new StringBuffer();
		sb.append(name + "("+phoneNo+"),");
		for (MedicineListModel dt : data) {

			sb.append(dt.getMedicineName() + ",\n" + dt.getNoOfDays() + " D,"
					+ "T: " + dt.getTimings());
			sb.append("\n");
		}

		sb.append("NectarIndia(9243449243).");
		Log.d("Name: ", sb.toString());
		return sb.toString();
	}

	public String formEmailData(){
		data = dbController.getAllMedicine();
		StringBuffer sb = new StringBuffer();
		sb.append(name+",");
		for (MedicineListModel dt : data) {

			sb.append(dt.getMedicineName() + ",\n" + dt.getNoOfDays() + " D,"
					+ "T: " + dt.getTimings());
			sb.append("\n");

			// Writing Contacts to log
			Log.d("Name: ", sb.toString());
		}
		return sb.toString();
	}

	public  void reflectChange(){

			dbController = new SQLiteController(mContext);
			dbController.open();
			data = dbController.getAllMedicine();
			medicineListAdapter = new MedicineListAdapter(mContext,
					R.layout.medicinelist_row_layout, data);
		if(medicineListView!=null)
			medicineListView.post(new Runnable() {
				@Override
				public void run() {
					if(data.size()>0) {
						medicineListView.setVisibility(View.VISIBLE);
						smsBtn.setVisibility(View.VISIBLE);
						shareBtn.setVisibility(View.VISIBLE);
						no_medicines_tv.setVisibility(View.GONE);

						medicineListView.setAdapter(medicineListAdapter);
						medicineListAdapter.setNotifyOnChange(true);
						medicineListAdapter.notifyDataSetChanged();
					}
				}
			});


	}
}
