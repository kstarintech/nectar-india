package com.nectarindia.org.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.nectarindia.org.R;
import com.nectarindia.org.activity.MainTabsActivity;
import com.nectarindia.org.adapter.AutocompleteAdapter;
import com.nectarindia.org.adapter.MedicineListAdapter;
import com.nectarindia.org.database.SQLiteController;

public class PrescriptionFragment extends Fragment {
	private String TAG = "PrescriptionFragment";
	Context mContext;
	AutoCompleteTextView acTextView;
	ImageButton addPrescription;
	EditText noofdaysET;
	CheckBox mrngChkBox, aftrNoonChkBox, nightChkBox;
	RadioButton beforeMeal, afterMeal, beforeMeal2, afterMeal2, beforeMeal3, afterMeal3;
	RadioButton mrngMealValue, aftrNoonMealValue, nightMealValue;
	RadioGroup mrngRadioGroup, afnoonRadioGroup, nightRadioGroup;
	String mrngMeal, aftrNoonMeal, nightMeal;
	String mrngCourse,  aftrnoonCourse, nightCourse;
	Button btnNext,btnBack;
	View rootView;
	private SQLiteController dbController;
	private static MedicineListAdapter medicineListAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		mContext = getActivity();
		rootView = inflater.inflate(R.layout.prescription_fragment, container, false);

		//Find TextView control
		acTextView = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView);
		acTextView.setAdapter(new AutocompleteAdapter(mContext, android.R.layout.simple_list_item_1));


		noofdaysET = (EditText) rootView.findViewById(R.id.noofdaysET);
		mrngChkBox = (CheckBox) rootView.findViewById(R.id.mrng);
		aftrNoonChkBox = (CheckBox) rootView.findViewById(R.id.aftrnoon);
		nightChkBox = (CheckBox) rootView.findViewById(R.id.night);

		addListenerOnChkWindows(rootView);

		//getScreenValues();
		beforeMeal = (RadioButton) rootView.findViewById(R.id.bmRadioButton);
		afterMeal = (RadioButton) rootView.findViewById(R.id.amRadioButton);

		beforeMeal2 = (RadioButton) rootView.findViewById(R.id.bmRadioButton2);
		afterMeal2 = (RadioButton) rootView.findViewById(R.id.amRadioButton2);

		beforeMeal3 = (RadioButton) rootView.findViewById(R.id.bmRadioButton3);
		afterMeal3 = (RadioButton) rootView.findViewById(R.id.amRadioButton3);

		mrngRadioGroup = (RadioGroup)rootView.findViewById(R.id.mrngRadioGroup);
		afnoonRadioGroup = (RadioGroup)rootView.findViewById(R.id.afnoonRadioGroup);
		nightRadioGroup = (RadioGroup)rootView.findViewById(R.id.nightRadioGroup);

		btnNext = (Button)rootView.findViewById(R.id.btn_next);
		btnBack = (Button)rootView.findViewById(R.id.btn_back);

		addPrescription = (ImageButton) rootView.findViewById(R.id.addPresciption);
		addPrescription.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(validateForm()) {
					insertScreenValues();
					resetValues();
					btnNext.setVisibility(View.VISIBLE);
				} else { }
			}
		});

		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainTabsActivity)getActivity()).setCurrentPosition(0);
			}
		});

		btnNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainTabsActivity)getActivity()).setCurrentPosition(2);
				((MainTabsActivity)getActivity()).reflectChange();
			}
		});

		return rootView;
	}

	public void addListenerOnChkWindows(View rootView) {



		mrngChkBox.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (((CheckBox) v).isChecked()) {
					Log.e(TAG, "mrngChkBox.isChecked: " + mrngChkBox.isChecked());
					/*beforeMeal.setVisibility(View.VISIBLE);
					afterMeal.setVisibility(View.VISIBLE);

						beforeMeal3.setVisibility(View.GONE);
					afterMeal3.setVisibility(View.GONE);
					beforeMeal2.setVisibility(View.GONE);
					afterMeal2.setVisibility(View.GONE);*/
					mrngRadioGroup.setVisibility(View.VISIBLE);
					/*afnoonRadioGroup.setVisibility(View.INVISIBLE);
					nightRadioGroup.setVisibility(View.INVISIBLE);*/
					if (beforeMeal.isChecked())
						Log.e(TAG, "beforeMeal.isChecked: " + beforeMeal.isChecked());
					if (afterMeal.isChecked())
						Log.e(TAG, "afterMeal.isChecked: " + afterMeal.isChecked());
				} else {
					Log.e(TAG, "mrngChkBox.isChecked: " + mrngChkBox.isChecked());
					/*beforeMeal.setVisibility(View.GONE);
					afterMeal.setVisibility(View.GONE);*/
					mrngRadioGroup.setVisibility(View.INVISIBLE);

				}

			}
		});

		aftrNoonChkBox.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (((CheckBox) v).isChecked()) {
					Log.e(TAG, "aftrNoonChkBox.isChecked: " + aftrNoonChkBox.isChecked());
					afnoonRadioGroup.setVisibility(View.VISIBLE);
					if (beforeMeal2.isChecked())
						Log.e(TAG, "beforeMeal2.isChecked: " + beforeMeal2.isChecked());
					if (afterMeal2.isChecked())
						Log.e(TAG, "afterMeal2.isChecked: " + afterMeal2.isChecked());
				} else {
					Log.e(TAG, "aftrNoonChkBox.isChecked: " + aftrNoonChkBox.isChecked());
					afnoonRadioGroup.setVisibility(View.INVISIBLE);

				}

			}
		});

		nightChkBox.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (((CheckBox) v).isChecked()) {
					Log.e(TAG, "nightChkBox.isChecked: " + nightChkBox.isChecked());
					nightRadioGroup.setVisibility(View.VISIBLE);
					if (beforeMeal3.isChecked())
						Log.e(TAG, "beforeMeal3.isChecked: " + beforeMeal3.isChecked());
					if (afterMeal3.isChecked())
						Log.e(TAG, "afterMeal3.isChecked: " + afterMeal3.isChecked());
				} else {
					Log.e(TAG, "nightChkBox.isChecked: " + nightChkBox.isChecked());
					nightRadioGroup.setVisibility(View.INVISIBLE);

				}

			}
		});

		/*mrngRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup group, int checkedId) {

				switch (checkedId) {
					case R.id.radio0:

						("sex", "Male");
						break;

					case R.id.radio1:
						editor.putString("sex", "Female");
						break;

				}
			}
		});*/
	}

	public void resetValues(){
		if(!acTextView.getText().toString().equals("") || acTextView.getText().length()>0){
			acTextView.setText("");
			acTextView.setHint(getResources().getString(R.string.ac_hint));
		}
		if(noofdaysET.getText().toString().equals("") || noofdaysET.getText().length()>0){
			noofdaysET.setText("");
		}
		mrngChkBox.setChecked(false);
		aftrNoonChkBox.setChecked(false);
		nightChkBox.setChecked(false);
		mrngRadioGroup.setVisibility(View.INVISIBLE);
		afnoonRadioGroup.setVisibility(View.INVISIBLE);
		nightRadioGroup.setVisibility(View.INVISIBLE);
	}

	public String insertScreenValues(){
		String resValue = null;
		mrngCourse = mrngChkBox.isChecked()?"1":"0";
		aftrnoonCourse = aftrNoonChkBox.isChecked()?"1":"0";
		nightCourse = nightChkBox.isChecked()?"1":"0";

		// get the selected radio button from the group
		int mrngSelectedOption = mrngRadioGroup.getCheckedRadioButtonId();
		// find the radiobutton by the previously returned id
		mrngMealValue = (RadioButton) rootView.findViewById(mrngSelectedOption);

		int aftrNoonSelectedOption = afnoonRadioGroup.getCheckedRadioButtonId();
		aftrNoonMealValue = (RadioButton) rootView.findViewById(aftrNoonSelectedOption);

		int nightSelectedOption = nightRadioGroup.getCheckedRadioButtonId();
		nightMealValue = (RadioButton) rootView.findViewById(nightSelectedOption);
		if(mrngChkBox.isChecked())
			mrngMeal = "/"+(mrngMealValue.getText().equals("Before Meal")?"BM":"AM");
		else
			mrngMeal = "";
		if(aftrNoonChkBox.isChecked())
			aftrNoonMeal = "/"+(aftrNoonMealValue.getText().equals("Before Meal")?"BM":"AM");
		else
			aftrNoonMeal = "";

		if(nightChkBox.isChecked())
			nightMeal = "/"+(nightMealValue.getText().equals("Before Meal")?"BM":"AM");
		else
			nightMeal = "";
		String timings = mrngCourse+mrngMeal+" - "+aftrnoonCourse+aftrNoonMeal
				+" - "+nightCourse+nightMeal;
		resValue = acTextView.getText()+" "+noofdaysET.getText()+" "+mrngCourse+mrngMeal+" "+aftrnoonCourse+aftrNoonMeal
				+" "+nightCourse+nightMeal;
		Log.e(TAG, "resValue: " + resValue);

		dbController = new SQLiteController(mContext);
		//dbController.open();

		dbController.insert(acTextView.getText().toString(), noofdaysET.getText().toString(), timings);
		return resValue;
	}

	public boolean validateForm(){
		boolean validate = true;
		if(acTextView.getText().toString().equals("")){
			acTextView.setError(getString(R.string.edittext_error_empty));
			validate = false;
		}else if(noofdaysET.getText().toString().equals("")){
			noofdaysET.setError(getString(R.string.edittext_error_empty));
			validate = false;
		}else if(!mrngChkBox.isChecked() && !aftrNoonChkBox.isChecked() && !nightChkBox.isChecked()){
			Toast.makeText(getActivity(), "At-least select one timing.!", Toast.LENGTH_SHORT).show();
			validate = false;
		}

		return validate;
	}
}
