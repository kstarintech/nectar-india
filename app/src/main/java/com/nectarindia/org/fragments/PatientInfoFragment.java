package com.nectarindia.org.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.nectarindia.org.R;
import com.nectarindia.org.activity.MainTabsActivity;
import com.nectarindia.org.util.Constants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatientInfoFragment extends Fragment {
	private String TAG = "PatientInfoFragment";
	EditText nameEt, emailEt, phoneEt, ageEt;
	Button btnNext, resetBtn;
	Context mContext;
	SharedPreferences pref;
	RadioGroup radioGroup;

	String valSex;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		final View rootView = inflater.inflate(R.layout.patient_fragment, container, false);

		nameEt = (EditText) rootView.findViewById(R.id.NameET);
		emailEt = (EditText) rootView.findViewById(R.id.emailET);
		phoneEt = (EditText) rootView.findViewById(R.id.PhoneET);
		ageEt = (EditText) rootView.findViewById(R.id.AgeET);
		radioGroup = (RadioGroup) rootView.findViewById(R.id.myRadioGroup);

		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// find which radio button is selected
				((RadioButton)rootView.findViewById(checkedId)).setSelected(true);
				if (checkedId == R.id.rad_female) {
					valSex = "Female";
				}
				else {
					valSex = "Male";
				}
			}

		});
		
		btnNext = (Button)rootView.findViewById(R.id.btn_next);
		btnNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String name = nameEt.getText().toString().trim();
				String phoneNo = phoneEt.getText().toString().trim();
				String email = emailEt.getText().toString().trim();

				if (!name.isEmpty() && !phoneNo.isEmpty()) {
					pref = mContext.getSharedPreferences(Constants.PREF_NAME, 0);
					SharedPreferences.Editor editor = pref.edit();

					editor.putString("PAT_NAME", name);
					editor.putString("PAT_PHONE", phoneNo);
					if(!email.isEmpty()) {
						if (!new EmailValidator().validate(email)) {
							emailEt.setError(mContext.getString(R.string.email_not_valid_msg));
						} else {
							editor.putString("PAT_EMAIL", email);
						}
					}
					editor.commit();
					((MainTabsActivity) getActivity()).setCurrentPosition(1);
				} else {
					Toast.makeText(mContext,
							"Please enter your Name and Phone number!", Toast.LENGTH_LONG)
							.show();
				}
			}
		});

		resetBtn = (Button)rootView.findViewById(R.id.reset_btn);
		resetBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				nameEt.setText("");
				phoneEt.setText("");
				emailEt.setText("");
				ageEt.setText("");

			}
		});

		return rootView;
	}

	public class EmailValidator {
		private Pattern pattern;
		private Matcher matcher;
		private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		public EmailValidator() {
			pattern = Patterns.EMAIL_ADDRESS; // Pattern.compile(EMAIL_PATTERN);
		}

		public boolean validate(final String hex) {
			matcher = pattern.matcher(hex);
			return matcher.matches();
		}
	}
}
